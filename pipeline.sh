#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=gpus
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --job-name=SynSort
#SBATCH --error=joblog_error_%A.txt
#SBATCH --output=joblog_output_%A.txt


#### helpers
function validate_resource() {
    local resource="$1"
    local flag="$2"
    local message="$3"
    local condition=0

    case "$flag" in
        file)
            if [[ ! -f "$resource" ]]; then
                condition=1
            fi
            ;;
        directory)
            if [[ ! -d "$resource" ]]; then
                condition=1
            fi
            ;;
        variable)
            if [[ -z "$resource" ]]; then
                condition=1
            fi
            ;;
        *)
            >&2 echo "Error: unknown flag: $flag, expected {file, directory, variable}."
            exit 1
            ;;
    esac

    if [[ "$condition" == 1 ]]; then
        >&2 echo "Error: ${message}"
        exit 1
    fi
}


function check_and_run() {
    local resource="$1"
    local function_to_run="$2"
    shift 2 # skip first two for remaining arguments
    
    if [[ ! -d "$resource" && ! -f "$resource" ]]; then
        >&2 echo "Resource $resource does not exist. Running stage: $function_to_run"
        $function_to_run "$@" # pass remaining arguments to function
        if [[ $? -ne 0 ]]; then
            >&2 echo "error: failed to complete $function_to_run"
            exit 1
        fi
    else
        >&2 echo "Resource $resource already exists. Skipping stage $function_to_run."
    fi
}


#### create experiment
function stage_experiment() {
    mkdir -p "$path_experiment"
}


#### demultiplexing
function stage_demultiplex() {
    mkdir -p "$path_fastq"
    ${path_bcl2fastq}/bcl2fastq \
    --runfolder-dir ${path_run} \
    --sample-sheet ${table_samples} \
    --output-dir ${path_fastq} \
    --loading-threads 8 \
    --processing-threads 8 \
    --writing-threads 8 \
    --no-lane-splitting
}


#### filtering adapters
function stage_filtering() {
    local fastq_in_r1="$1"
    local fastq_in_r2="$2"
    local fastq_out_r1="$3"
    local fastq_out_r2="$4"
    local output_tag="$5"

    ${path_fastp}/fastp \
    --in1="$fastq_in_r1" \
    --in2="$fastq_in_r2" \
    --out1="$fastq_out_r1" \
    --out2="$fastq_out_r2" \
    --compression=4 \
    --adapter_sequence="$adapter_r1" \
    --adapter_sequence_r2="$adapter_r2" \
    --trim_poly_x \
    --poly_x_min_len 8 \
    --low_complexity_filter \
    --complexity_threshold=30 \
    --average_qual=20 \
    --length_required=18 \
    --reads_to_process=0 \
    --thread=16 \
    --json="${output_tag}_fastp.json" \
    --html="${output_tag}_fastp.html" \
    2> "${output_tag}_fastp.txt"
}


#### alignment
function stage_alignment() {
    local fastq_r1="$1"
    local fastq_r2="$2"
    local path_bams="$3"
    local path_counts="$4"
    local sample_name="$5"

    cd "$path_bams"

    ${path_star}/STAR \
    --runMode alignReads \
    --runThreadN 16 \
    --genomeDir "$genome_index" \
    --readFilesIn "$file_r2" "$file_r1" \
    --readFilesCommand zcat \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMstrandField intronMotif \
    --outSAMattributes All \
    --outSAMattrIHstart 0 \
    --outBAMsortingThreadN 16 \
    --soloType CB_UMI_Simple \
    --soloCBtype Sequence \
    --soloCBwhitelist "$table_barcodes" \
    --soloCBstart "$cb_start" \
    --soloCBlen "$cb_len" \
    --soloUMIstart "$umi_start" \
    --soloUMIlen "$umi_len" \
    --soloBarcodeReadLength "$r1_len" \
    --soloBarcodeMate 0 \
    --soloCBmatchWLtype 1MM \
    --soloStrand Unstranded \
    --soloFeatures GeneFull_Ex50pAS \
    --soloMultiMappers Uniform \
    --soloUMIdedup 1MM_All \
    --soloUMIfiltering - \
    --soloOutFileNames Solo.out/ features.tsv barcodes.tsv matrix.mtx \
    --soloCellReadStats Standard \
    > "${path_bams}/${sample_name}_logStar.txt"

    mv Aligned.sortedByCoord.out.bam "${path_bams}/${sample_name}.bam"
    mv Log.final.out "${path_bams}/${sample_name}_logReads.txt"
    mv SJ.out.tab "${path_bams}/${sample_name}_SJ.txt"
    mv Solo.out "${path_counts}/${sample_name}_Solo"
    
    rm -f *.out
    ${path_htslib}/samtools index "${path_bams}/${sample_name}.bam"   
}


#### set environment
path_biotools="/gpfs/scic/software/biotools"
path_bcl2fastq="${path_biotools}/bcl2fastq2/bin"
path_fastp="${path_biotools}/fastp"
path_star="${path_biotools}/STAR-2.7.11b/Linux_x86_64_static"
path_htslib="${path_biotools}/htslib/bin"


#### read and validate configuration file
config_file="$1"
validate_resource "$config_file" "file" "configuration file is not valid but required."
source "$config_file"
validate_resource "$experiment_name" "variable" "experiment name is not valid, but required."
validate_resource "$path_project" "directory" "project path is not valid, but required."
validate_resource "$path_run" "directory" "sequencing run path is not valid, but required."
validate_resource "$table_samples" "file" "sample sheet file is not valid, but required."
validate_resource "$table_barcodes" "file" "barcodes file is not valid, but required."
validate_resource "$genome_index" "directory" "STAR genome index is not valid, but required."
validate_resource "$genome_gtf" "file" "GTF annotation file is not valid, but required."
validate_resource "$r1_len" "variable" "Read 1 length is not valid, but required."
validate_resource "$cb_start" "variable" "Sample index start on  R1 is not valid, but required."
validate_resource "$cb_len" "variable" "Sample index length on  R1 is not valid, but required."
validate_resource "$umi_start" "variable" "UMI start on  R1 is not valid, but required."
validate_resource "$umi_len" "variable" "UMI length on  R1 is not valid, but required."
validate_resource "$adapter_r1" "variable" "Adapter on R1 is not valid, but required."
validate_resource "$adapter_r2" "variable" "Adapter on R2 is not valid, but required."


#### prepare stage paths
path_experiment="${path_project}/${experiment_name}"
path_fastq="${path_experiment}/fastq"
path_fastq_clean="${path_experiment}/fastq_clean"
path_bams="${path_experiment}/bams"
path_counts="${path_experiment}/counts"


#### pipleine stages
check_and_run "$path_experiment" "stage_experiment"
check_and_run "$path_fastq" "stage_demultiplex"


#### per sample processing
# mkdir -p "$path_fastq_clean"
mkdir -p "$path_bams"
mkdir -p "$path_counts"
for file_r1 in "${path_fastq}"/*_R1_001.fastq.gz
do
    sample_name=$(basename "$file_r1" | sed -E 's/(_S[0-9]+)?(_L[0-9]+)?_R1_001.fastq.gz//')
    file_r2=$(ls "${path_fastq}/${sample_name}"*_R2_001.fastq.gz 2> /dev/null)
    
    validate_resource "$sample_name" "variable" "sample name is not valid, but required."
    validate_resource "$file_r1" "file" "fastq R1 is not valid, but required."
    validate_resource "$file_r2" "file" "fastq R2 is not valid, but required."
    
    if [[ "$sample_name" == "Undetermined" ]]; then
        continue
    fi

    #### stage filtering
    file_r1_clean="${path_fastq_clean}/${sample_name}_R1_clean.fastq.gz"
    file_r2_clean="${path_fastq_clean}/${sample_name}_R2_clean.fastq.gz"
    output_tag="${path_fastq_clean}/${sample_name}"
    # check_and_run "$file_r1_clean" "stage_filtering" "$file_r1" "$file_r2" "$file_r1_clean" "$file_r2_clean" "$output_tag"

    #### statge alignment
    file_bam="${path_bams}/${sample_name}.bam"
    # check_and_run "$file_bam" "stage_alignment" "$file_r1_clean" "$file_r2_clean" "$path_bams" "$path_counts" "$sample_name"
    check_and_run "$file_bam" "stage_alignment" "$file_r1" "$file_r2" "$path_bams" "$path_counts" "$sample_name"

done

echo "pipeline succeffully done."